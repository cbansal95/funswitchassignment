function restore_options() {
    chrome.storage.sync.get({
        wordList: ['videos']
    }, function (items) {
        document.getElementById('textList').innerHTML ="";
        for (let i = 0; i < items.wordList.length; i++) {
            document.getElementById('textList').innerHTML += "<li>" + items.wordList[i]  + "</li>";
        }

    });
}


function add_option() {
    chrome.storage.sync.get({
        wordList: ['videos']
    }, function (items) {
        let new_entry = document.getElementById('newText').value;
        if (new_entry && !items.wordList.includes(new_entry)) {
            items.wordList.push(new_entry.toLowerCase());
            document.getElementById('newText').value = null;
            chrome.storage.sync.set({
                wordList: items.wordList
            }, function () {
                alert('New search term added')
                restore_options();
            })
        } else {
            alert('Enter valid/unique text')
            document.getElementById('newText').value = null
        }
    })

}
document.addEventListener('DOMContentLoaded', restore_options);
const elem = document.getElementById('addText');
elem.addEventListener('click', add_option)